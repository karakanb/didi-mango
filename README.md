# didi-mango

## Data4Help

### Access to individuals
```mermaid
sequenceDiagram
    participant T as Third Party
    participant D as TrackMe
    participant I as Individual
    T->>D: I would like to have I's data.
    D->>I: Do you consent data access?
    alt I allows
        I->>D: Yes I do.
        D->>T: Here is I's data.
    else I rejects
        I->>D: No I don't.
        D->>T: I rejected your request, no data.
    end
```

### Access to anonymized data
```mermaid
sequenceDiagram
    participant T as Third Party
    participant D as TrackMe
    T->>D: I would like to have all these data.
    alt number <= 1000
        D->>T: We cannot give this data.
    else number > 1000
        D->>T: Here is your data.
    end
```


### Subscribe
```mermaid
sequenceDiagram
    participant T as Third Party
    participant D as TrackMe
    T->>D: Subscribe me for the data I had consent.
    D->>T: Sure.
    loop New Data
        D->>T: Here is new data.
    end
```


## AutomatedSOS

### Check health
```mermaid
sequenceDiagram
    participant T as Ambulance
    participant D as TrackMe
    participant I as Individual
    loop Every second
        D->>I: Give me the health data.
        opt Values < threshold
            D->>T: Send an ambulance to here.
        end
    end
```


## Track4Run

### Define path
```mermaid
sequenceDiagram
    participant O as Organizer
    participant T as Track4Run
    O->>T: Define this path: x, y, z
    activate T
    T->>O: Path successfully defined.
    deactivate T
```


### Enroll to path
```mermaid
sequenceDiagram
    participant P as Participant
    participant T as Track4Run
    participant D as Data4Help
    P->>T: Enroll to this path: x
    T->>D: I would like to have I's data.
    D->>P: Do you consent data access?
    alt Participant allows
        P->>D: Yes I do.
        D->>T: Here is Participant's data.
        T->>P: Successfully enrolled to x.
    else Participant rejects
        P->>D: No I don't.
        D->>T: Participant rejected your request, no data.
        T->>P: You cannot enroll without data acess.
    end
    
```


### See position of runner
```mermaid
sequenceDiagram
    participant S as Spectator
    participant T as Track4Run
    participant D as TrackMe
    participant P as Participant
    S->>T: I want to see Participant's position.
    activate T
    T->>D: I want to see Participant's position.
    activate D
    D->>P: What is your position?
    activate P
    P->>D: This is my position.
    deactivate P
    D->>T: Here is Participant's position.
    deactivate D
    T->>S: Here is Participant's position.
    deactivate T
```